'use strict';
let fs = require('fs'); // модуль для работы с файловой системой

const mqtt = require('mqtt');
const bot = require('./bot.js');
let ownerTelegramChatId = '369459962';
let ownerTelegramChatId2 = 'azaza';
let message = 'Бот запущен';

bot.sendMessage(ownerTelegramChatId, message, { parse_mode: 'HTML' });


let url = 'mqtt://localhost:1883';
let client = mqtt.connect(url, { username: 'fakeEpd', password: 'fakeEpd' });
let topicName = ['/temperature', '/humidity', '/temperatureOut', '/temperatureWater', '/dateEsp', '/stateEsp']
client.on('connect', function () {
  for (let i = 0; i < topicName.length; i++) {
    client.subscribe(topicName);
  };
})



client.on('close', function () {
  console.log('emulator DISCONNECT');
})




let lastValues = {
  humidityRoom: 'null',
  temperatureRoom: 'null',
  temperatureOut: 'null',
  temperatureWater: 'null',
  dateEsp: null,
  currDif: null,
  stateEsp: null,
  alert: {
    alertState: 0,
    alertCurrDif: 0,
    alertTemperatureRoom: 0,
    alertHumidityRoom: 0,
  },
  alertMessage: {
    alertState: 'err nothing',
    alertCurrDif: 'current different 0',
    alertTemperatureRoom: 'temperature Ok',
    alertHumidityRoom: 'humidity Ok',
    alertTemperatureOut: 'temperature out Ok',
  }
};

let averagingValues = {
  averagingValueTemperatureRoom: [0],
  averagingValueHumidityRoom: [0],
  averagingValueTemperatureOut: [0],
  averagingValueTemperatureWater: [0],
};

client.on('message', onMqttMessage);
function onMqttMessage(topic, message) {
  if (topic === '/temperature') {
    lastValues.temperatureRoom = JSON.parse(message);
    if (lastValues.temperatureRoom === 21474836.470000 && lastValues.alert.alertTemperatureRoom === 0) {
      lastValues.alertMessage.alertTemperatureRoom = 'temperature room`s sensor cut';
      lastValues.alert.alertTemperatureRoom = 2;
      bot.sendMessage(ownerTelegramChatId, lastValues.alertMessage.alertTemperatureRoom, { parse_mode: 'HTML' });
    } else if (lastValues.temperatureRoom !== 21474836.470000 && lastValues.alert.alertTemperatureRoom === 2) {
      lastValues.alertMessage.alertTemperatureRoom = 'temperature room`s sensor Ok';
      lastValues.alert.alertTemperatureRoom = 0;
      averagingValues.averagingValueTemperatureRoom = averagingValue(lastValues.temperatureRoom, averagingValues.averagingValueTemperatureRoom);
    } else {
      averagingValues.averagingValueTemperatureRoom = averagingValue(lastValues.temperatureRoom, averagingValues.averagingValueTemperatureRoom);
    }
  } else if (topic === '/humidity') {
    lastValues.humidityRoom = JSON.parse(message);
    averagingValues.averagingValueHumidityRoom = averagingValue(lastValues.humidityRoom, averagingValues.averagingValueHumidityRoom);
  } else if (topic === '/temperatureOut') {
    lastValues.temperatureOut = JSON.parse(message);
    averagingValues.averagingValueTemperatureOut = averagingValue(lastValues.temperatureOut, averagingValues.averagingValueTemperatureOut);
  } else if (topic === '/temperatureWater') {
    lastValues.temperatureWater = JSON.parse(message);
    averagingValues.averagingValueTemperatureWater = averagingValue(lastValues.temperatureWater, averagingValues.averagingValueTemperatureWater);
  } else if (topic === '/dateEsp') {
    lastValues.dateEsp = JSON.parse(message);
  } else if (topic === '/stateEsp') {
    lastValues.stateEsp = JSON.parse(message);
  }
};

function averagingValue(msg, arr) {
  if (arr.length < 300) {
    arr.push(msg);
  } else {
    arr.shift();
    arr.push(msg);
  };
  return arr;
}


function publishAveragingValue() {
  let topicNameAveragingValues = Object.keys(averagingValues);
  let arrValuesAveragingForMessage = Object.values(averagingValues);
  for (let i = 0; i < topicNameAveragingValues.length; i++) {
    let topic = '/' + topicNameAveragingValues[i];
    let msg = (arrValuesAveragingForMessage[i].reduce((sum, current) => sum + current, 0)) / arrValuesAveragingForMessage[i].length
    let msgSend = msg.toFixed(2);
    client.publish(topic, msgSend, 0);
  };
};




function onAlertTemp() {
  let messageAlert = null;
  if (lastValues.temperatureRoom > 36 && lastValues.alert.alertTemperatureRoom === 0) {
    messageAlert = 'тепрература высокая = ' + lastValues.temperatureRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
    lastValues.alert.alertTemperatureRoom = 1;
  } else if (lastValues.temperatureRoom < 10 && lastValues.alert.alertTemperatureRoom === 0) {
    messageAlert = 'тепрература низкая = ' + lastValues.temperatureRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
    lastValues.alert.alertTemperatureRoom = 1;
  } else if (lastValues.temperatureRoom > 12 && lastValues.temperatureRoom < 34 && lastValues.alert.alertTemperatureRoom === 1) {
    lastValues.alert.alertTemperatureRoom = 0;
    messageAlert = 'температура в норме = ' + lastValues.temperatureRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  }
};

function onAlertHum() {
  let messageAlert = null;
  if (lastValues.humidityRoom > 60 && lastValues.alert.alertHumidityRoom === 0) {
    lastValues.alert.alertHumidityRoom = 1;
    messageAlert = 'влажность высокая = ' + lastValues.humidityRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  } else if (lastValues.humidityRoom < 15 && lastValues.alert.alertHumidityRoom === 0) {
    lastValues.alert.alertHumidityRoom = 1;
    messageAlert = 'влажность низкая = ' + lastValues.humidityRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  } else if (lastValues.humidityRoom > 17 && lastValues.humidityRoom < 58 && lastValues.alert.alertHumidityRoom === 1) {
    lastValues.alert.alertHumidityRoom = 0;
    messageAlert = 'влажность в норме = ' + lastValues.humidityRoom;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  };
};


function controlTime() {
  let currentDate = new Date();
  let messageAlert = null;
  lastValues.currDif = currentDate.getTime() / 1000 - JSON.parse(lastValues.dateEsp);
  if (lastValues.currDif > 60 && lastValues.alert.alertCurrDif === 0) {
    lastValues.alert.alertCurrDif = 1;
    messageAlert = 'err esp, current difference = ' + lastValues.currDif + ', alert dif = ' + lastValues.alert.alertCurrDif;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  } else if (lastValues.currDif < 60 && lastValues.alert.alertCurrDif === 1) {
    lastValues.alert.alertCurrDif = 0;
    messageAlert = 'esp GOOD, current difference = ' + lastValues.currDif;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  }
};

function checkEsp() {
  let messageAlert = null;
  if (lastValues.alert.alertCurrDif === 1 || lastValues.alert.alertTemperatureRoom === 1 || lastValues.alert.alertHum === 1) {
    lastValues.alert.alertState = 1;
  } else if (lastValues.alert.alertCurrDif === 0 && lastValues.alert.alertTemperatureRoom === 0 && lastValues.alert.alertHumidityRoom === 0) {
    lastValues.alert.alertState = 0;
  }
  if (lastValues.alert.alertState === 1 && lastValues.alert.alertStateOnOff === 0) {
    messageAlert = 'err esp, ON alert!!!';
    lastValues.alert.alertStateOnOff = 1;
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  } else if (lastValues.alert.alertState === 0 && lastValues.alert.alertStateOnOff === 1) {
    lastValues.alert.alertStateOnOff = 0;
    messageAlert = 'esp GOOD, state GOOD, alert OFF!!! ';
    bot.sendMessage(ownerTelegramChatId, messageAlert, { parse_mode: 'HTML' });
  }
};




bot.onText(/\/temp/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, lastValues.temperatureRoom);
});

bot.onText(/\/hum/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, lastValues.humidityRoom);
});

bot.onText(/\/currDif/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, lastValues.currDif);
});

bot.onText(/\/alert/, (msg) => {
  const chatId = msg.chat.id;
  let botSendMsg = 'alert = ' + lastValues.alert.alertState + ', alertTemperatureRoom = ' + lastValues.alert.alertTemperatureRoom + ', alertHumidity = ' + lastValues.alert.alertHumidityRoom + ', current diff = ' + lastValues.alert.alertCurrDif;
  bot.sendMessage(chatId, botSendMsg);
});

bot.onText(/\/water/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, lastValues.temperatureWater);
});

bot.onText(/\/out/, (msg) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, lastValues.temperatureOut);
});

function logLoop() { // сборка текста для вывода на экран и в лог фаил
  let currentDate = Date.now() + 21600000;
  let text = '';
  text +=  new Date(currentDate) + '\r\n\n';
  text += 'temperature Room: ' + lastValues.temperatureRoom + ' гр. С' + '\r\n';
  text += 'temperature Out: ' + lastValues.temperatureOut + ' гр. С' + '\r\n\n';
  text += 'humidity Room: ' + lastValues.humidityRoom + ' гр. С' + '\r\n';
//  console.log(text); //вывод в консоле
  fs.appendFileSync('log1.txt', text + '\n'); //запись в фаил

};




function makeDate () {
  let currentDate = Date.now() + (6*1000*60*60);
  let currentDateFotmated = (new Date (currentDate));
  let currentDateStringify = JSON.stringify(currentDateFotmated);
  let arrDateQuotes = currentDateStringify.split('"');
  let arrDate = arrDateQuotes[1].split('T');
  let readyTime = arrDate[1].split('.');
  let msg = arrDate[0] + ' ' + readyTime[0];
  client.publish('/date', msg, 0);
}




let daysStage = {
  dateStart: Date.parse(new Date(2020,9,1)),
  dateStartGrow: Date.parse(new Date(2020,9,4)),
  dateStartPreFlow: Date.parse(new Date(2020,10,5)),
  dateStartFlow: Date.parse(new Date(2020,11,4)),
  dateEndFlow: Date.parse(new Date(null)), 
}

function makeDaysInterval (){
  let currentDate = Date.now() + (6*1000*60*60);
  let msg = 'growreport do not begun';
  if (daysStage.dateStart === 0) { // rep d`t start
    client.publish('/days', msg, 0);
  } else if (daysStage.dateStartGrow === 0) { // rep start preGrow
    msg = "Started " + Math.floor((currentDate - daysStage.dateStart)/86400000) + " days ago.";
    client.publish('/days', msg, 0);
  } else if (daysStage.dateStartPreFlow === 0) { // rep Grow
    msg = "Started: " + Math.floor((currentDate - daysStage.dateStart)/86400000) + " days ago. Growth: " +  Math.floor((currentDate - daysStage.dateStartGrow)/86400000);
    client.publish('/days', msg, 0);
  } else if (daysStage.dateStartFlow === 0) {  // rep pre flow
    msg = "Started: " + Math.floor((currentDate - daysStage.dateStart)/86400000) + " days ago. Growth: " +  Math.floor((daysStage.dateStartPreFlow - daysStage.dateStartGrow)/86400000) + ', Pre-Flowering: ' +  Math.floor((currentDate - daysStage.dateStartPreFlow)/86400000);
    client.publish('/days', msg, 0);
  } else if (daysStage.dateEndFlow === 0) {
    msg = "Started: " + Math.floor((currentDate - daysStage.dateStart)/86400000) + " days ago. Growth: " +  Math.floor((daysStage.dateStartPreFlow - daysStage.dateStartGrow)/86400000) + ', Pre-Flowering: ' +  Math.floor((daysStage.dateStartFlow - daysStage.dateStartPreFlow)/86400000) + ', Flowering: ' +  Math.floor((currentDate - daysStage.dateStartFlow)/86400000);
    client.publish('/days', msg, 0);    
  } else if (daysStage.dateEndFlow !== 0) {
    msg = "Report is over, total days:" + Math.floor((daysStage.dateEndFlow - daysStage.dateStart)/86400000)
    client.publish('/days', msg, 0);  
  }
}



function onServer() {
  onAlertTemp();
  onAlertHum();
  checkEsp();
  controlTime();
  //logLoop();
}

setInterval(publishAveragingValue, 300 * 1000);
setInterval(onServer, 1 * 1000);
setInterval(makeDate, 1 * 1000);
setInterval(makeDaysInterval, 1*1000);
module.exports = client;






